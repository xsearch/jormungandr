package benchmarks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;

public class IndexWorker extends Thread {
    private AtomicInteger fileCounter;
    private ArrayList<String> filenames;
    private IndexWriter idxWriter;

    public IndexWorker(AtomicInteger fileCounter, ArrayList<String> filenames, IndexWriter idxWriter) {
        this.fileCounter = fileCounter;
        this.filenames = filenames;
        this.idxWriter = idxWriter;
    }

    @Override
    public void run() {
        int i;

        i = fileCounter.getAndIncrement();
        while (i < filenames.size()) {
            try {
                File file = new File(filenames.get(i));
                Document document = new Document();
            
                Field contentField = new TextField("content", new InputStreamReader(new FileInputStream(file)));
                Field filepathField = new StringField("filepath", filenames.get(i), Field.Store.YES);

                document.add(filepathField);
                document.add(contentField);

                idxWriter.addDocument(document);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            i = fileCounter.getAndIncrement();
        }
    }
}
