package benchmarks;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.ConcurrentMergeScheduler;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.store.ByteBuffersDirectory;
import org.apache.lucene.store.Directory;

public class NSMemIndexBenchmark {
    public static final String MSG = String.join("Usage: java MemIndexBenchmark.jar <source file> ",
                                                 "<total size in bytes> <number of index threads> ",
                                                 "<RAM buffer size (MB)> <search file>");
    public static final int MAX_BUFFERED_DOCS = 1048576;
    public static final int NUM_MERGE_THREADS = 1;

    private String sourceFile;
    private int numIndexThreads;
    private int numMergerThreads;
    private int RAMBufferSize;
    private String searchFile;
    private double execTime;
    private double initTime;
    private double searchTime;
    private long rc;

    public NSMemIndexBenchmark(String sourceFile,
                               int numIndexThreads,
                               int RAMBufferSize,
                               String searchFile) {
        this.sourceFile = sourceFile;
        this.numIndexThreads = numIndexThreads;
        this.numMergerThreads = NUM_MERGE_THREADS;
        this.RAMBufferSize = RAMBufferSize;
        this.searchFile = searchFile;
        this.execTime = 0.0;
        this.initTime = 0.0;
        this.searchTime = 0.0;
        this.rc = 0;
    }

    protected ArrayList<String> readSearchTerms() {
        ArrayList<String> terms = new ArrayList<String>();
        FileReader filReader;
        BufferedReader bufReader;
        String line;

        try {
            filReader = new FileReader(searchFile);
            bufReader = new BufferedReader(filReader);

            line = bufReader.readLine();
            while (line != null) {
                terms.add(line);
                line = bufReader.readLine();
            }

            filReader.close();
        } catch(IOException e) {
            e.printStackTrace();
        }

        return terms;
    }

    protected ArrayList<String> readFilenames() {
        ArrayList<String> filenames = new ArrayList<String>();
        FileReader filReader;
        BufferedReader bufReader;
        String line;

        try {
            filReader = new FileReader(sourceFile);
            bufReader = new BufferedReader(filReader);
            
            line = bufReader.readLine();
            while (line != null) {
                filenames.add(line);
                line = bufReader.readLine();
            }

            filReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return filenames;
    }

    public void runBenchmark() {
        ArrayList<IndexWorker> workers = new ArrayList<IndexWorker>();
        ArrayList<String> filenames;
        ArrayList<String> terms;
        Analyzer analyzer;
        ArrayList<Analyzer> analyzers = new ArrayList<Analyzer>();
        Directory idxDirectory;
        ArrayList<Directory> idxDirectories = new ArrayList<Directory>();
        IndexWriterConfig idxWriterConfig;
        ConcurrentMergeScheduler idxScheduler;
        IndexWriter idxWriter;
        ArrayList<IndexWriter> idxWriters = new ArrayList<IndexWriter>();
        DirectoryReader idxDirectoryReader;
        ArrayList<DirectoryReader> idxDirectoryReaders = new ArrayList<DirectoryReader>();
        IndexSearcher idxSearcher;
        ArrayList<IndexSearcher> idxSearchers = new ArrayList<IndexSearcher>();
        QueryParser parser;
        ArrayList<QueryParser> parsers = new ArrayList<QueryParser>();
        Query query;
        ScoreDoc[] hits;
        Document doc;
        AtomicInteger fileCounter = new AtomicInteger(0);
        long start, end;

        filenames = readFilenames();
        terms = readSearchTerms();

        try {
            start = System.nanoTime();
            for (int i = 0; i < numIndexThreads; i++) {
                analyzer = new WhitespaceAnalyzer();
                analyzers.add(analyzer);
                idxDirectory = new ByteBuffersDirectory();
                idxDirectories.add(idxDirectory);
                idxWriterConfig = new IndexWriterConfig(analyzer);
                idxWriterConfig.setSimilarity(new ClassicSimilarity());
                idxWriterConfig.setMaxBufferedDocs(MAX_BUFFERED_DOCS);
                idxWriterConfig.setRAMBufferSizeMB(RAMBufferSize);
                idxScheduler = new ConcurrentMergeScheduler();
                idxScheduler.setMaxMergesAndThreads(numMergerThreads+1, numMergerThreads);
                idxWriterConfig.setMergeScheduler(idxScheduler);
                idxWriter = new IndexWriter(idxDirectory, idxWriterConfig);
                idxWriters.add(idxWriter);
            }
            end = System.nanoTime();
            initTime = Double.valueOf(end - start).doubleValue() / 1000000000.0;

            start = System.nanoTime();
            for (int i = 0; i < numIndexThreads; i++) {
                workers.add(new IndexWorker(fileCounter, filenames, idxWriters.get(i)));
            }

            for (IndexWorker worker : workers) {
                worker.start();
            }

            for (IndexWorker worker : workers) {
                try {
                    worker.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            end = System.nanoTime();
            execTime = Double.valueOf(end - start).doubleValue() / 1000000000.0;
            
            for (int i = 0; i < numIndexThreads; i++) {
                idxWriters.get(i).close();
            }

            start = System.nanoTime();

            for (int i = 0; i < numIndexThreads; i++) {
                idxDirectoryReader = DirectoryReader.open(idxDirectories.get(i));
                idxDirectoryReaders.add(idxDirectoryReader);
                idxSearcher = new IndexSearcher(idxDirectoryReader);
                idxSearchers.add(idxSearcher);
                parser = new QueryParser("content", analyzers.get(i));
                parsers.add(parser);
            }
            
            for (int j = 0; j < terms.size(); j++) {
                System.out.print("- " + terms.get(j) + " : [ ");
                
                for (int i = 0; i < numIndexThreads; i++) {
                    query = parsers.get(i).parse(terms.get(j));
                    hits = idxSearchers.get(i).search(query, 4096).scoreDocs;
                    rc += hits.length;
                    for (int k = 0; k < hits.length; k++) {
                        doc = idxSearchers.get(i).doc(hits[k].doc);
                        System.out.print(doc.get("filepath") + " ");
                    }
                }
                
                System.out.println("]");
            }

            for (int i = 0; i < numIndexThreads; i++) {
                idxDirectoryReaders.get(i).close();
                idxDirectories.get(i).close();
            }
            end = System.nanoTime();
            searchTime = Double.valueOf(end - start).doubleValue() / 1000000000.0;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public double getExecTime() {
        return execTime;
    }

    public double getInitTime() {
        return initTime;
    }

    public double getSearchTime() {
        return searchTime;
    }

    public long getRC() {
        return rc;
    }

    public static void main(String[] args) {
        String sourceFile;
        long totalSize;
        int numIndexThreads;
        int RAMBufferSize;
        String searchFile;
        NSMemIndexBenchmark benchmark;

        double execTime, initTime, searchTime;
        long rc;
        
        if (args.length != 5) {
            System.out.println(MSG);
            System.exit(1);
        }

        sourceFile = args[0];
        totalSize = Long.valueOf(args[1]).longValue();
        numIndexThreads = Integer.valueOf(args[2]).intValue();
        RAMBufferSize = Integer.valueOf(args[3]).intValue();
        searchFile = args[4];

        System.out.println("Started indexing, storing and searching " + totalSize + " bytes of data:");
        benchmark = new NSMemIndexBenchmark(sourceFile, numIndexThreads, RAMBufferSize, searchFile);
        benchmark.runBenchmark();
        execTime = benchmark.getExecTime();
        initTime = benchmark.getInitTime();
        searchTime = benchmark.getSearchTime();
        rc = benchmark.getRC();
        
        System.out.println("Finished indexing, storing and searching " + totalSize + " bytes of data:");
        System.out.println("- number of indexing threads: " + numIndexThreads);
        System.out.println("- RAM buffer size: " + RAMBufferSize + " MiB");
        System.out.println("- total initialization time: " + initTime);
        System.out.println("- total execution time: " + execTime);
        System.out.println("- total search time: " + searchTime);
        System.out.println("- throughput: " + ((totalSize / (1024 * 1024)) / execTime) + " MiB/sec");
        System.out.println("Return code: " + rc);

        System.exit(0);
    }
}
