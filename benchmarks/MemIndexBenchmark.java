package benchmarks;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.index.ConcurrentMergeScheduler;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.store.ByteBuffersDirectory;
import org.apache.lucene.store.Directory;

public class MemIndexBenchmark {
    public static final String MSG = String.join("Usage: java MemIndexBenchmark.jar <source file> ",
                                          "<total size in bytes> <number of index threads> ",
                                          "<RAM buffer size (MB)> <search term>");
    public static final int MAX_BUFFERED_DOCS = 1048576;
    public static final int NUM_MERGE_THREADS = 4;

    private String sourceFile;
    private int numIndexThreads;
    private int numMergerThreads;
    private int RAMBufferSize;
    private String searchTerm;
    private double execTime;
    private long rc;

    public MemIndexBenchmark(String sourceFile,
                             int numIndexThreads,
                             int RAMBufferSize,
                             String searchTerm) {
        this.sourceFile = sourceFile;
        this.numIndexThreads = numIndexThreads;
        this.numMergerThreads = NUM_MERGE_THREADS;
        this.RAMBufferSize = RAMBufferSize;
        this.searchTerm = searchTerm;
        this.execTime = 0.0;
        this.rc = 0;
    }

    protected ArrayList<String> readFilenames(String sourceFile) {
        ArrayList<String> filenames = new ArrayList<String>();
        FileReader filReader;
        BufferedReader bufReader;
        String line;

        try {
            filReader = new FileReader(sourceFile);
            bufReader = new BufferedReader(filReader);
            
            line = bufReader.readLine();
            while (line != null) {
                filenames.add(line);
                line = bufReader.readLine();
            }

            filReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return filenames;
    }

    public void runBenchmark() {
        ArrayList<IndexWorker> workers = new ArrayList<IndexWorker>();
        ArrayList<String> filenames;
        Analyzer analyzer;
        Directory idxDirectory;
        IndexWriterConfig idxWriterConfig;
        ConcurrentMergeScheduler idxScheduler;
        IndexWriter idxWriter;
        AtomicInteger pos = new AtomicInteger(0);
        long start, end;

        filenames = readFilenames(sourceFile);

        try {
            analyzer = new WhitespaceAnalyzer();
            idxDirectory = new ByteBuffersDirectory();
            idxWriterConfig = new IndexWriterConfig(analyzer);
            idxWriterConfig.setSimilarity(new ClassicSimilarity());
            idxWriterConfig.setMaxBufferedDocs(MAX_BUFFERED_DOCS);
            idxWriterConfig.setRAMBufferSizeMB(RAMBufferSize);
            idxScheduler = new ConcurrentMergeScheduler();
            idxScheduler.setMaxMergesAndThreads(numMergerThreads+4, numMergerThreads);
            idxWriterConfig.setMergeScheduler(idxScheduler);
            idxWriter = new IndexWriter(idxDirectory, idxWriterConfig);

            start = System.nanoTime();
            for (int i = 0; i < numIndexThreads; i++) {
                workers.add(new IndexWorker(pos, filenames, idxWriter));
            }

            for (IndexWorker worker : workers) {
                worker.start();
            }

            for (IndexWorker worker : workers) {
                try {
                    worker.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            end = System.nanoTime();
            execTime = Double.valueOf(end - start).doubleValue() / 1000000000.0;

            idxWriter.close();

            DirectoryReader idxDirectoryReader = DirectoryReader.open(idxDirectory);
            IndexSearcher idxSearcher = new IndexSearcher(idxDirectoryReader);
            QueryParser parser = new QueryParser("content", analyzer);
            Query query = parser.parse(searchTerm);
            ScoreDoc[] hits = idxSearcher.search(query, 1024).scoreDocs;
            rc = hits.length;
            
            idxDirectoryReader.close();
            idxDirectory.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public double getExecTime() {
        return execTime;
    }

    public long getRC() {
        return rc;
    }

    public static void main(String[] args) {
        String sourceFile;
        long totalBytes;
        int numIndexThreads;
        int RAMBufferSize;
        String searchTerm;
        MemIndexBenchmark benchmark;

        double execTime;
        long rc;
        
        if (args.length != 5) {
            System.out.println(MSG);
            System.exit(1);
        }

        sourceFile = args[0];
        totalBytes = Long.valueOf(args[1]).longValue();
        numIndexThreads = Integer.valueOf(args[2]).intValue();
        RAMBufferSize = Integer.valueOf(args[3]).intValue();
        searchTerm = args[4];

        benchmark = new MemIndexBenchmark(sourceFile, numIndexThreads, RAMBufferSize, searchTerm);
        benchmark.runBenchmark();
        execTime = benchmark.getExecTime();
        rc = benchmark.getRC();
        
        System.out.println("Finished indexing!");
        System.out.println("- Total number of bytes: " + totalBytes);
        System.out.println("- Number of index threads: " + numIndexThreads);
        System.out.println("- RAM buffer size: " + RAMBufferSize + " MB");
        System.out.println("- Total execution time: " + execTime + " seconds");
        System.out.println("- Throughput: " + ((totalBytes / (1024 * 1024)) / execTime) + " MiB/sec");
        System.out.println("Return code: " + rc);

        System.exit(0);
    }
}
