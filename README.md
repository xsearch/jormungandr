**Illinois Institute of Technology**  
**Data-Intensive Distributed Systems Laboratory**  
**Project Omega - Jormungandr Benchmark**  
Maintainer: Alexandru Iulian Orhean (aorhean@hawk.iit.edu)

Scalable and high-performance Apache Lucene based search engine benchmarks.

# Setup
Extract the lucene archive and copy the directory to $HOME/.local/lib
